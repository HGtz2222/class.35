#include <stdio.h>

void Func2(int c) {
  printf("%d\n", c);
}

void Func() {
  int a = 10;
  int b = 20;
  int c = a + b;
  Func2(c);
}

int main() {
  Func();
  return 0;
}
